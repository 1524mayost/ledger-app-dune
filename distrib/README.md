
Installing
----------

You need `pip`. On Debian, you can run:

```
sudo apt-get install python-pip
pip install --no-cache-dir ledgerblue 
```

Now, you can plug your Ledger Nano S, enter the PIN, and call the script:

```
./install.sh
```

You will be asked for your PIN again, maybe to confirm the removal of
a previous version, and then to confirm the installation, with a PIN again.

Uninstalling
------------

Use the `uninstall.sh` script:

```
./uninstall.sh
```
