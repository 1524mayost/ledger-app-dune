
#include "apdu_baking.h"

#include "apdu.h"
#include "baking_auth.h"
#include "globals.h"
#include "os_cx.h"
#include "protocol.h"
#include "to_string.h"
#include "ui_nano_s.h"

#include <string.h>

#define G global.u.baking

static bool reset_ok(void) {
  UPDATE_NVRAM(ram, {
      for(int i=0;i<CACHE_NVRAM.nbakers;i++){
        hwm* hwm = &ram->bakers[i].hwm;
        hwm->main.highest_level = G.reset_level;
        hwm->main.had_endorsement = false;
        hwm->test.highest_level = G.reset_level;
        hwm->test.had_endorsement = false;
      }
    });

  // Send back the response, do not restart the event loop
  delayed_send(finalize_successful_send(0));
  return true;
}

size_t handle_apdu_reset(uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);

  uint8_t *dataBuffer = G_io_apdu_buffer + OFFSET_CDATA;
  uint32_t dataLength = G_io_apdu_buffer[OFFSET_LC];
  
  if (dataLength != sizeof(int)) {
    THROW(EXC_WRONG_LENGTH_FOR_INS);
  }
  level_t const lvl = READ_UNALIGNED_BIG_ENDIAN(level_t, dataBuffer);
  if (!is_valid_level(lvl)) THROW(EXC_PARSE_ERROR);

  G.reset_level = lvl;

  register_ui_callback(0, number_to_string_indirect32, &G.reset_level);

  static const char *const reset_prompts[] = {
    PROMPT("Reset HWM"),
    NULL,
  };
  ui_prompt(reset_prompts, reset_ok, delay_reject);
}



size_t send_word_big_endian(size_t tx, uint32_t word) {
  char word_bytes[sizeof(word)];

  memcpy(word_bytes, &word, sizeof(word));

  // endian.h functions do not compile
  uint32_t i = 0;
  for (; i < sizeof(word); i++) {
    G_io_apdu_buffer[i + tx] = word_bytes[sizeof(word) - i - 1];
  }

  return tx + i;
}

size_t handle_apdu_all_hwm( uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);
    
  size_t tx = 0;
  for(int i=0;i<CACHE_NVRAM.nbakers;i++){
    baker* baker=&CACHE_NVRAM.bakers[i];
    tx = send_word_big_endian(tx, baker->hwm.main.highest_level);
    tx = send_word_big_endian(tx, baker->hwm.test.highest_level);
    tx = send_word_big_endian(tx, baker->main_chain_id.v);
  }
  return finalize_successful_send(tx);
}

size_t handle_apdu_main_hwm(uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);
    
  size_t tx = 0;
  for(int i=0;i<CACHE_NVRAM.nbakers;i++){
    tx = send_word_big_endian(tx, CACHE_NVRAM.bakers[i].hwm.main.highest_level);
  }
  return finalize_successful_send(tx);
}


size_t handle_apdu_query_auth_key(uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);

  size_t tx = 0;
  for(int i=0;i<CACHE_NVRAM.nbakers;i++){
    baker* baker=&CACHE_NVRAM.bakers[i];
    uint8_t const length = baker->baking_key.bip32_path.length;
    G_io_apdu_buffer[tx++] = length;
    
    for (uint8_t j = 0; j < length; ++j) {
      tx = send_word_big_endian(tx,
                                baker->baking_key.bip32_path.components[j]);
    }
  }
  return finalize_successful_send(tx);
}

size_t handle_apdu_query_auth_key_with_curve(uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);
    
  size_t tx = 0;
  for(int i=0;i<CACHE_NVRAM.nbakers;i++){
    baker* baker=&CACHE_NVRAM.bakers[i];
    
    uint8_t const curve_code = curve_to_curve_code(baker->baking_key.curve);
    if (curve_code == DUNE_NO_CURVE) THROW(EXC_REFERENCED_DATA_NOT_FOUND);
    G_io_apdu_buffer[tx++] = curve_code;
    
    uint8_t const length = baker->baking_key.bip32_path.length;
    G_io_apdu_buffer[tx++] = length;
    for (uint8_t j = 0; j < length; ++j) {
      tx = send_word_big_endian(tx,
                                baker->baking_key.bip32_path.components[j]);
    }
  }

  return finalize_successful_send(tx);
}

size_t handle_apdu_deauthorize(uint8_t instruction) {
  if( !IS_BAKING_MODE() ) handle_apdu_error(instruction);
  
  if (READ_UNALIGNED_BIG_ENDIAN(uint8_t, &G_io_apdu_buffer[OFFSET_P1]) != 0)
    THROW(EXC_WRONG_PARAM);
  if (READ_UNALIGNED_BIG_ENDIAN(uint8_t, &G_io_apdu_buffer[OFFSET_LC]) != 0)
    THROW(EXC_PARSE_ERROR);
  
  UPDATE_NVRAM(ram, {
      memset(&ram->bakers, 0, sizeof(ram->bakers));
      ram->nbakers=0;
    });

  return finalize_successful_send(0);
}







static bool set_baking_mode_cont(void)
{
  uint8_t new_value = global.u.set_baking_mode.new_baking_mode ;
  if( NOT_FLAG ( new_value , BAKING_FLAG ) )  require_pin();
  UPDATE_NVRAM(ram, {
      ram->baking_mode = new_value;
      if( NOT_FLAG ( new_value , BAKING_FLAG ) ){
        memset(&ram->bakers, 0, sizeof(ram->bakers));
        ram->nbakers=0;
      }
    });
  G_io_apdu_buffer[0] = new_value;
  size_t tx = 1;
  delayed_send(finalize_successful_send(tx));
  return true;
}

static const size_t TYPE_INDEX = 0;

size_t set_baking_mode(uint8_t new_mode) {

  global.u.set_baking_mode.new_baking_mode = new_mode ;

  if ( global.baking_auth.new_data.baking_mode == new_mode ) {
    G_io_apdu_buffer[0] = new_mode;
    size_t tx = 1;
    return finalize_successful_send(tx);
  }

  if( IS_MODE_ALWAYS() ) THROW(EXC_SECURITY);

  if( NOT_FLAG ( new_mode , BAKING_FLAG ) ){
    const char *const prompt[] = {
      PROMPT("Switch to Wallet"),
      NULL,
    };
    REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "After PIN");
    ui_prompt( prompt,
               set_baking_mode_cont,
               delay_reject);
  } else {

    #define BAKING_MODE_INDEX 0
    #define WATERMARK_MODE_INDEX 1
    static const char *const prompt[] = {
      PROMPT("Switch to Baking"),
      PROMPT("Watermarks in"),
      NULL,
    };

    if ( HAS_FLAG ( new_mode , RAMWAT_FLAG ) )
      REGISTER_STATIC_UI_VALUE(WATERMARK_MODE_INDEX, "Volatile Memory");
    else
      REGISTER_STATIC_UI_VALUE(WATERMARK_MODE_INDEX, "Persistent Memory");

    if( HAS_FLAG ( new_mode , ALWAYS_FLAG ) ){
      REGISTER_STATIC_UI_VALUE(BAKING_MODE_INDEX, "Always");
    } else {
      REGISTER_STATIC_UI_VALUE(BAKING_MODE_INDEX, "For now");
    }

    ui_prompt( prompt,
               set_baking_mode_cont,
               delay_reject);
  }
}


size_t handle_set_baking_mode(__attribute__((unused)) uint8_t ins) {

  size_t const cdata_size = G_io_apdu_buffer[OFFSET_LC] ;
  uint8_t new_mode =
   cdata_size == 0 ?
    ( IS_BAKING_MODE() ? 0 : BAKING_FLAG ) :
    G_io_apdu_buffer[OFFSET_CDATA] ;

  return set_baking_mode( new_mode );
}

// Only for compatibility
size_t handle_set_baking_always(__attribute__((unused)) uint8_t ins) {
  return set_baking_mode( BAKING_FLAG | ALWAYS_FLAG );
}
