#pragma once

#include "apdu.h"

extern size_t handle_apdu_get_public_key(uint8_t instruction);

extern size_t provide_pubkey(uint8_t *const io_buffer,
                             cx_ecfp_public_key_t const *const pubkey);

