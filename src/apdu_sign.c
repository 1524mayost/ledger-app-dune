#include "apdu_sign.h"

#include "apdu.h"
#include "baking_auth.h"
#include "lib-base58.h"
#include "lib-blake2.h"
#include "globals.h"
#include "keys.h"
#include "types.h"
#include "protocol.h"
#include "to_string.h"
#include "ui_nano_s.h"

#include "cx.h"

#include <string.h>

#define G global.u.sign

#define B2B_BLOCKBYTES 128

static void conditional_init_hash_state
(
 blake2b_hash_state_t *const state
 )
{
  check_null(state);
  if (!state->initialized) {
    b2b_init(&state->state, SIGN_HASH_SIZE);
    state->initialized = true;
  }
}

static void blake2b_incremental_hash
(
 /*in/out*/ uint8_t *const out, size_t const out_size,
 /*in/out*/ size_t *const out_length,
 /*in/out*/ blake2b_hash_state_t *const state
 )
{
  check_null(out);
  check_null(out_length);
  check_null(state);

  uint8_t *current = out;
  while (*out_length > B2B_BLOCKBYTES) {
    if (current - out > (int)out_size) THROW(EXC_MEMORY_ERROR);
    conditional_init_hash_state(state);
    b2b_update(&state->state, current, B2B_BLOCKBYTES);
    *out_length -= B2B_BLOCKBYTES;
    current += B2B_BLOCKBYTES;
  }
  // TODO use circular buffer at some point
  memmove(out, current, *out_length);
}

static inline void blake2b_finish_hash
(
 /*out*/ uint8_t *const out, size_t const out_size,
 /*in/out*/ uint8_t *const buff, size_t const buff_size,
 /*in/out*/ size_t *const buff_length,
 /*in/out*/ blake2b_hash_state_t *const state
 )
{
  check_null(out);
  check_null(buff);
  check_null(buff_length);
  check_null(state);

  conditional_init_hash_state(state);
  blake2b_incremental_hash(buff, buff_size, buff_length, state);
  b2b_update(&state->state, buff, *buff_length);
  b2b_final(&state->state, out, out_size);
}


static inline void clear_data(void) {
  memset(&G, 0, sizeof(G));
  G.maybe_parsed_baking_data.v.baker = -1;
}

static int perform_signature(bool const on_hash, bool const send_hash) {
  PRINTF_STACK_SIZE("perform_signature");
  if( IS_BAKING_MODE() ){
    
    if (G.maybe_parsed_baking_data.vstate == VALID_VSTATE)
      write_high_water_mark(&G.maybe_parsed_baking_data.v);
    
  } else {
    if ( on_hash && G.hash_only ) {
    memcpy(G_io_apdu_buffer, G.final_hash, sizeof(G.final_hash));
    clear_data();
    return finalize_successful_send(sizeof(G.final_hash));
  }
  }
    
    size_t tx = 0;
    if (send_hash && on_hash) {
    memcpy(&G_io_apdu_buffer[tx], G.final_hash, sizeof(G.final_hash));
    tx += sizeof(G.final_hash);
  }
    
    uint8_t const *const data = on_hash ? G.final_hash : G.message_data;
    size_t const data_length = on_hash ?
      sizeof(G.final_hash) : G.message_data_length;
    tx += sign(&G_io_apdu_buffer[tx],
      MAX_SIGNATURE_SIZE, &G.key, data, data_length);
    
    clear_data();
    return finalize_successful_send(tx);
  }    

static bool sign_without_hash_ok(void) {
  delayed_send(perform_signature(true, false));
  return true;
}

static bool sign_with_hash_ok(void) {
  delayed_send(perform_signature(true, true));
  return true;
}

static bool sign_reject(void) {
  clear_data();
  delay_reject();
  return true; // Return to idle
}

//  return parse_operations(out, in, in_size,
//                          key->curve, &key->bip32_path);


__attribute__((noreturn)) static void prompt_register_delegate
(
 ui_callback_t const ok_cb,
 ui_callback_t const cxl_cb
 )
{
  static const size_t TYPE_INDEX = 0;
  static const size_t ADDRESS_INDEX = 1;
  static const size_t FEE_INDEX = 2;

  static const char *const prompts[] = {
    PROMPT("Register"),
    PROMPT("Address"),
    PROMPT("Fee"),
    NULL,
  };

  if (G.maybe_ops.vstate != VALID_VSTATE) THROW(EXC_MEMORY_ERROR);

  REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "as delegate?");
  register_ui_callback(ADDRESS_INDEX,
                       bip32_path_with_curve_to_pkh_string, &G.key);
  register_ui_callback(FEE_INDEX,
                       microtez_to_string_indirect, &G.maybe_ops.v.total_fee);

  ui_prompt(prompts, ok_cb, cxl_cb);
}

int find_authorized_baker(bip32_path_with_curve_t const *const key)
{
  for(int i = 0; i < CACHE_NVRAM.nbakers ; i++){
    if( bip32_path_with_curve_eq(key, &CACHE_NVRAM.bakers[i].baking_key) ) return i;
  }
  return (-1);
}

size_t baking_sign_complete(bool const send_hash) {
  switch (G.magic_number) {
  case MAGIC_BYTE_BLOCK:
  case MAGIC_BYTE_BAKING_OP:
    if (G.maybe_parsed_baking_data.vstate == VALID_VSTATE) {
      guard_baking_authorized(&G.maybe_parsed_baking_data.v, &G.key);
      return perform_signature(true, send_hash);
    } else {
      THROW(EXC_PARSE_ERROR);
    }
    break;

  case MAGIC_BYTE_UNSAFE_OP:
    {
      // Must be self-delegation signed by the *authorized* baking key
      if (G.maybe_ops.vstate == VALID_VSTATE &&
          find_authorized_baker( &G.key ) >= 0 &&

          // ops->signing is generated from G.bip32_path and G.curve
          COMPARE(&G.maybe_ops.v.operation.source,
                  &G.maybe_ops.v.signing) == 0 &&
          COMPARE(&G.maybe_ops.v.operation.destination,
                  &G.maybe_ops.v.signing) == 0
          ) {
        ui_callback_t const ok_c =
          send_hash ? sign_with_hash_ok : sign_without_hash_ok;
        prompt_register_delegate(ok_c, sign_reject);
      }
      THROW(EXC_SECURITY);
    }
  case MAGIC_BYTE_UNSAFE_OP2:
  case MAGIC_BYTE_UNSAFE_OP3:
  default:
    THROW(EXC_PARSE_ERROR);
  }
}

static bool sign_unsafe_ok(void) {
  delayed_send(perform_signature(false, false));
  return true;
}

#define ORIGINATION_PROMPTS(DEL_STR) {          \
    PROMPT("Confirm"),                          \
    PROMPT("Amount"),                           \
    PROMPT("Fee"),                              \
    PROMPT("Source"),                           \
    PROMPT("Manager"),                          \
    PROMPT(DEL_STR),                            \
    PROMPT("Storage"),                          \
    NULL,                                       \
  }
#define ORIGINATION_CH_PROMPTS(DEL_STR) {       \
    PROMPT("Confirm"),                          \
    PROMPT("Amount"),                           \
    PROMPT("Fee"),                              \
    PROMPT("Source"),                           \
    PROMPT("Manager"),                          \
    PROMPT(DEL_STR),                            \
    PROMPT("Storage"),                          \
    PROMPT("Code Hash"),                        \
    NULL,                                       \
  }


// include decimal point and terminating null
#define MAX_NUMBER_CHARS (MAX_INT_DIGITS + 2)

bool prompt_transaction
(
 struct parsed_operation_group const *const ops,
 bip32_path_with_curve_t const *const key,
 ui_callback_t ok, ui_callback_t cxl
 )
{
  check_null(ops);
  check_null(key);

  switch (ops->operation.tag) {
  default:
    THROW(EXC_PARSE_ERROR);

  case OPERATION_TAG_PROPOSAL:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t SOURCE_INDEX = 1;
      static const uint32_t PERIOD_INDEX = 2;
      static const uint32_t PROTOCOL_HASH_INDEX = 3;

      static const char *const proposal_prompts[] = {
        PROMPT("Confirm"),
        PROMPT("Source"),
        PROMPT("Period"),
        PROMPT("Protocol"),
        NULL,
      };

      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string, &ops->operation.source);
      register_ui_callback(PERIOD_INDEX,
                           number_to_string_indirect32,
                           &ops->operation.proposal.voting_period);
      register_ui_callback(PROTOCOL_HASH_INDEX,
                           protocol_hash_to_string,
                           ops->operation.proposal.protocol_hash);

      REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Proposal");
      ui_prompt(proposal_prompts, ok, cxl);
    }

  case OPERATION_TAG_BALLOT:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t SOURCE_INDEX = 1;
      static const uint32_t PROTOCOL_HASH_INDEX = 2;
      static const uint32_t PERIOD_INDEX = 3;

      static const char *const ballot_prompts[] = {
        PROMPT("Confirm Vote"),
        PROMPT("Source"),
        PROMPT("Protocol"),
        PROMPT("Period"),
        NULL,
      };

      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string,
                           &ops->operation.source);
      register_ui_callback(PROTOCOL_HASH_INDEX,
                           protocol_hash_to_string,
                           ops->operation.ballot.protocol_hash);
      register_ui_callback(PERIOD_INDEX,
                           number_to_string_indirect32,
                           &ops->operation.ballot.voting_period);

      switch (ops->operation.ballot.vote) {
      case BALLOT_VOTE_YEA:
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Yea");
        break;
      case BALLOT_VOTE_NAY:
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Nay");
        break;
      case BALLOT_VOTE_PASS:
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Pass");
        break;
      }

      ui_prompt(ballot_prompts, ok, cxl);
    }

  case OPERATION_TAG_ORIGINATION:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t AMOUNT_INDEX = 1;
      static const uint32_t FEE_INDEX = 2;
      static const uint32_t SOURCE_INDEX = 3;
      static const uint32_t DESTINATION_INDEX = 4;
      static const uint32_t DELEGATE_INDEX = 5;
      static const uint32_t STORAGE_INDEX = 6;
      static const uint32_t CODE_HASH_INDEX = 7;

      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string,
                           &ops->operation.source);
      register_ui_callback(DESTINATION_INDEX,
                           parsed_contract_to_string,
                           &ops->operation.destination);
      register_ui_callback(FEE_INDEX,
                           microtez_to_string_indirect,
                           &ops->total_fee);
      register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                           &ops->total_storage_limit);


      if ( ops->operation.flags & ORIGINATION_FLAG_HAS_SCRIPT ) {
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Orig. Contract");
      } else {
        if (!(ops->operation.flags & ORIGINATION_FLAG_SPENDABLE)) return false;
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Orig. Account");
      }

      register_ui_callback(AMOUNT_INDEX,
                           microtez_to_string_indirect,
                           &ops->operation.amount);

      char const *const *prompts;
      bool const delegatable =
        ops->operation.flags & ORIGINATION_FLAG_DELEGATABLE;
      bool const has_delegate =
        ops->operation.delegate.curve_code != DUNE_NO_CURVE;
      bool const has_code_hash =
        ops->operation.flags & ORIGINATION_FLAG_HAS_SCRIPT_HASH;

      if (delegatable) {
        if (has_code_hash) {
          static const char *const my_prompts[] = ORIGINATION_CH_PROMPTS("Delegate");
          prompts = my_prompts;
        } else {
          static const char *const my_prompts[] = ORIGINATION_PROMPTS("Delegate");
          prompts = my_prompts;
        }
      } else if (has_delegate) {
        if (has_code_hash) {
          static const char *const my_prompts[] = ORIGINATION_CH_PROMPTS("Fixed Delegate");
          prompts = my_prompts;
        } else {
          static const char *const my_prompts[] = ORIGINATION_PROMPTS("Fixed Delegate");
          prompts = my_prompts;
        }
      } else {
        if (has_code_hash) {
          static const char *const my_prompts[] = ORIGINATION_CH_PROMPTS("Delegation");
          prompts = my_prompts;
        } else {
          static const char *const my_prompts[] = ORIGINATION_PROMPTS("Delegation");
          prompts = my_prompts;
        }
      }

      if (has_delegate) {
        register_ui_callback(DELEGATE_INDEX, parsed_contract_to_string,
                             &ops->operation.delegate);
      } else if (delegatable) {
        REGISTER_STATIC_UI_VALUE(DELEGATE_INDEX, "Any");
      } else {
        REGISTER_STATIC_UI_VALUE(DELEGATE_INDEX, "Disabled");
      }

      if (has_code_hash) {
        register_ui_callback(CODE_HASH_INDEX,
                             code_hash_to_string,
                             ops->operation.proposal.protocol_hash);
      }

      ui_prompt(prompts, ok, cxl);
    }
  case OPERATION_TAG_DELEGATION:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t FEE_INDEX = 1;
      static const uint32_t SOURCE_INDEX = 2;
      static const uint32_t DESTINATION_INDEX = 3;
      static const uint32_t STORAGE_INDEX = 4;

      register_ui_callback(SOURCE_INDEX, parsed_contract_to_string,
                           &ops->operation.source);
      register_ui_callback(DESTINATION_INDEX, parsed_contract_to_string,
                           &ops->operation.destination);
      register_ui_callback(FEE_INDEX,
                           microtez_to_string_indirect, &ops->total_fee);
      register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                           &ops->total_storage_limit);

      static const char *const withdrawal_prompts[] = {
        PROMPT("Withdraw"),
        PROMPT("Fee"),
        PROMPT("Source"),
        PROMPT("Delegate"),
        PROMPT("Storage"),
        NULL,
      };
      static const char *const delegation_prompts[] = {
        PROMPT("Confirm"),
        PROMPT("Fee"),
        PROMPT("Source"),
        PROMPT("Delegate"),
        PROMPT("Storage"),
        NULL,
      };

      REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Delegation");

      bool withdrawal = ops->operation.destination.originated == 0 &&
        ops->operation.destination.curve_code == DUNE_NO_CURVE;

      ui_prompt(withdrawal ? withdrawal_prompts : delegation_prompts, ok, cxl);
    }

  case OPERATION_TAG_TRANSACTION:
  case OPERATION_TAG_COLLECT_CALL:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t AMOUNT_INDEX = 1;
      static const uint32_t FEE_INDEX = 2;
      static const uint32_t SOURCE_INDEX = 3;
      static const uint32_t DESTINATION_INDEX = 4;
      static const uint32_t STORAGE_INDEX = 5;

      static const char *const transaction_prompts[] = {
        PROMPT("Confirm"),
        PROMPT("Amount"),
        PROMPT("Fee"),
        PROMPT("Source"),
        PROMPT("Destination"),
        PROMPT("Storage"),
        NULL,
      };

      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string, &ops->operation.source);
      register_ui_callback(DESTINATION_INDEX, parsed_contract_to_string,
                           &ops->operation.destination);
      register_ui_callback(FEE_INDEX,
                           microtez_to_string_indirect, &ops->total_fee);
      register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                           &ops->total_storage_limit);
      register_ui_callback(AMOUNT_INDEX,
                           microtez_to_string_indirect, &ops->operation.amount);

      if ( ops->operation.tag == OPERATION_TAG_COLLECT_CALL ) {
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Collect Call");
      } else if ( ops->operation.flags & TRANSACTION_FLAG_HAS_PARAM ) {
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Contract Call");
      } else {
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Transaction");
      }

      ui_prompt(transaction_prompts, ok, cxl);
    }

  case OPERATION_TAG_DUNE_MANAGER:
    {
      if ( ops->operation.flags & DUNE_MANAGE_ACCOUNTS_FLAG ) {
        static const uint32_t TYPE_INDEX = 0;
        static const uint32_t SOURCE_INDEX = 1;
        static const uint32_t FEE_INDEX = 2;
        static const uint32_t STORAGE_INDEX = 3;

        static const char *const prompts[] = {
          PROMPT("Confirm Dune"),
          PROMPT("Source"),
          PROMPT("Fee"),
          PROMPT("Storage Limit"),
          NULL,
        };

        register_ui_callback(SOURCE_INDEX,
                             parsed_contract_to_string, &ops->operation.source);
        register_ui_callback(FEE_INDEX,
                             microtez_to_string_indirect,
                             &ops->total_fee);
        register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                             &ops->total_storage_limit);

        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Manage Accounts");
        ui_prompt(prompts, ok, cxl);

      } else {

        static const uint32_t TYPE_INDEX = 0;
        static const uint32_t LEVEL_INDEX = 1;
        static const uint32_t PROTO_INDEX = 2;
        static const uint32_t PARAM_INDEX = 3;
        static const uint32_t SOURCE_INDEX = 4;
        static const uint32_t FEE_INDEX = 5;
        static const uint32_t STORAGE_INDEX = 6;

        static const char *const activate_prompts[] = {
          PROMPT("Confirm Dune"),
          PROMPT("Level"),
          PROMPT("Protocol"),
          PROMPT("Parameters"),
          PROMPT("Source"),
          PROMPT("Fee"),
          PROMPT("Storage Limit"),
          NULL,
        };

        register_ui_callback(LEVEL_INDEX,
                             number_to_string_indirect32,
                             &ops->operation.proposal.voting_period);
        register_ui_callback(SOURCE_INDEX,
                             parsed_contract_to_string, &ops->operation.source);
        register_ui_callback(FEE_INDEX,
                             microtez_to_string_indirect,
                             &ops->total_fee);
        register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                             &ops->total_storage_limit);

        if ( ops->operation.flags & DUNE_ACTIVATE_FLAG_HAS_PROTO ) {
          register_ui_callback(PROTO_INDEX,
                               protocol_hash_to_string,
                               ops->operation.proposal.protocol_hash);
        } else {
          REGISTER_STATIC_UI_VALUE(PROTO_INDEX, "None");
        }
        if ( ops->operation.flags & DUNE_ACTIVATE_FLAG_HAS_PARAM ) {
          REGISTER_STATIC_UI_VALUE(PARAM_INDEX, "YES");
        } else {
          REGISTER_STATIC_UI_VALUE(PARAM_INDEX, "NO");
        }
        REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Activate");
        ui_prompt(activate_prompts, ok, cxl);
      }
    }

  case OPERATION_TAG_NONE:
    {
      static const uint32_t TYPE_INDEX = 0;
      static const uint32_t SOURCE_INDEX = 1;
      static const uint32_t FEE_INDEX = 2;
      static const uint32_t STORAGE_INDEX = 3;

      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string, &ops->operation.source);
      register_ui_callback(FEE_INDEX,
                           microtez_to_string_indirect, &ops->total_fee);

      // Parser function guarantees this has a reveal
      static const char *const reveal_prompts[] = {
        PROMPT("Reveal Key"),
        PROMPT("Key"),
        PROMPT("Fee"),
        PROMPT("Storage"),
        NULL,
      };

      REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "To Blockchain");
      register_ui_callback(STORAGE_INDEX, number_to_string_indirect64,
                           &ops->total_storage_limit);
      register_ui_callback(SOURCE_INDEX,
                           parsed_contract_to_string, &ops->operation.source);

      ui_prompt(reveal_prompts, ok, cxl);
    }
  }
}

size_t wallet_sign_complete(uint8_t instruction) {
  PRINTF_STACK_SIZE("wallet_sign_complete");
  static size_t const TYPE_INDEX = 0;
  static size_t const HASH_INDEX = 1;

  static const char *const parse_fail_prompts[] = {
    PROMPT("Unrecognized"),
    PROMPT("Sign Hash"),
    NULL,
  };

  REGISTER_STATIC_UI_VALUE(TYPE_INDEX, "Operation");

  if (instruction == INS_SIGN_UNSAFE) {
    static const char *const prehashed_prompts[] = {
      PROMPT("Pre-hashed"),
      PROMPT("Sign Hash"),
      NULL,
    };

    G.message_data_as_buffer.bytes = (uint8_t *)&G.message_data;
    G.message_data_as_buffer.size = sizeof(G.message_data);
    G.message_data_as_buffer.length = G.message_data_length;
    // Base58 encoding of 32-byte hash is 43 bytes long.
    register_ui_callback(HASH_INDEX,
                         buffer_to_base58, &G.message_data_as_buffer);
    ui_prompt(prehashed_prompts, sign_unsafe_ok, sign_reject);
  } else {
    ui_callback_t const ok_c =
      instruction == INS_SIGN_WITH_HASH ?
      sign_with_hash_ok : sign_without_hash_ok;

    switch (G.magic_number) {
    case MAGIC_BYTE_BLOCK:
    case MAGIC_BYTE_BAKING_OP:
    default:
      THROW(EXC_PARSE_ERROR);
    case MAGIC_BYTE_UNSAFE_OP:
      if (G.maybe_ops.vstate != VALID_VSTATE
          || !prompt_transaction(&G.maybe_ops.v, &G.key, ok_c, sign_reject)) {
        goto unsafe;
      }

    case MAGIC_BYTE_UNSAFE_OP2:
    case MAGIC_BYTE_UNSAFE_OP3:
      goto unsafe;
    }
  unsafe:
    G.message_data_as_buffer.bytes = (uint8_t *)&G.final_hash;
    G.message_data_as_buffer.size = sizeof(G.final_hash);
    G.message_data_as_buffer.length = sizeof(G.final_hash);
    // Base58 encoding of 32-byte hash is 43 bytes long.
    register_ui_callback(HASH_INDEX,
                         buffer_to_base58, &G.message_data_as_buffer);
    ui_prompt(parse_fail_prompts, ok_c, sign_reject);
  }
}

#define P1_FIRST 0x00
#define P1_NEXT 0x01
#define P1_HASH_ONLY_NEXT 0x03 // You only need it once
#define P1_LAST_MARKER 0x80

#define UPDATE_VALID_STATE(VSTATE, VALUE)       \
  switch(VSTATE) {                              \
  case(INVALID_VSTATE):                         \
    break;                                      \
  case (UNKNOWN_VSTATE):                        \
    if (VALUE) {                                \
      VSTATE = VALID_VSTATE;                    \
    } else {                                    \
      VSTATE = INVALID_VSTATE;                  \
    }                                           \
    break;                                      \
  case (VALID_VSTATE):                          \
    VSTATE = INVALID_VSTATE;                    \
    break;                                      \
  }

size_t handle_apdu_sign(bool const enable_hashing, bool const enable_parsing, uint8_t const instruction) {
  PRINTF_STACK_SIZE("handle_apdu_sign");
  if( IS_BAKING_MODE() && instruction == INS_SIGN_UNSAFE )
    return handle_apdu_error(instruction);

  
  uint8_t *const buff = &G_io_apdu_buffer[OFFSET_CDATA];
  uint8_t const p1 = G_io_apdu_buffer[OFFSET_P1];
  uint8_t const buff_size = G_io_apdu_buffer[OFFSET_LC];
  if (buff_size > MAX_APDU_SIZE) THROW(EXC_WRONG_LENGTH_FOR_INS);

  bool last = (p1 & P1_LAST_MARKER) != 0;
  switch (p1 & ~P1_LAST_MARKER) {
  case P1_FIRST:
    clear_data();
    read_bip32_path(&G.key.bip32_path, buff, buff_size);
    G.key.curve =
      curve_code_to_curve(G_io_apdu_buffer[OFFSET_CURVE]);
    return finalize_successful_send(0);
        
  case P1_NEXT:
    if (G.key.bip32_path.length == 0) {
      THROW(EXC_WRONG_LENGTH_FOR_INS);
    }
    break;
  default:
    THROW(EXC_WRONG_PARAM);
  }
  
  if (enable_parsing) {
    if (G.magic_number == MAGIC_BYTE_INVALID) {
      G.magic_number = get_magic_byte(buff, buff_size);
    }
    if( IS_BAKING_MODE()
#ifdef HAVE_U2F
        // No automatic signing from browser inputs
        && G_io_apdu_media != IO_APDU_MEDIA_U2F
#endif
        ){
      // Keep trying to parse baking data unless we already did so successfully.
      UPDATE_VALID_STATE(G.maybe_parsed_baking_data.vstate,
                         parse_baking_data(&G.maybe_parsed_baking_data.v, buff, buff_size));
    }

    // Keep trying to parse operations.
    UPDATE_VALID_STATE(G.maybe_ops.vstate,
                       parse_operations(&G.maybe_ops.v, buff, buff_size,
                                        G.key.curve, &G.key.bip32_path));
  }
  
  if (enable_hashing) {
    // Hash contents of *previous* message (which may be empty).
    blake2b_incremental_hash(
                             G.message_data, sizeof(G.message_data),
                             &G.message_data_length,
                             &G.hash_state);
  }
  
  if (G.message_data_length + buff_size > sizeof(G.message_data)) {
    THROW(EXC_PARSE_ERROR);
  }
    
  memmove(G.message_data + G.message_data_length, buff, buff_size);
  G.message_data_length += buff_size;
  
  if (last) {
    if (enable_hashing) {
      // Hash contents of *this* message and then get the final hash value.
      blake2b_incremental_hash(
                               G.message_data, sizeof(G.message_data),
                               &G.message_data_length,
                               &G.hash_state);
      blake2b_finish_hash(
                          G.final_hash, sizeof(G.final_hash),
                          G.message_data, sizeof(G.message_data),
                          &G.message_data_length,
                          &G.hash_state);
    }

    return
      IS_BAKING_MODE() ?
      baking_sign_complete( instruction == INS_SIGN_WITH_HASH ) :
      wallet_sign_complete(instruction) ;
      
  } else {
    return finalize_successful_send(0);
  }
}
      
