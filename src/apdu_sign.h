#pragma once

#include "apdu.h"

extern size_t handle_apdu_sign(bool const enable_hashing, bool const enable_parsing, uint8_t const instruction);
