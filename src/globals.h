#pragma once

#include "lib-blake2.h"
#include "types.h"

#include "bolos_target.h"

void init_globals(void);

#define MAX_APDU_SIZE 230  // Maximum number of bytes in a single APDU

// Our buffer must accommodate any remainder from hashing and the next message at once.
#define DUNE_BUFSIZE (BLAKE2B_BLOCKBYTES + MAX_APDU_SIZE)

#define PRIVATE_KEY_DATA_SIZE 32

#define MAX_SIGNATURE_SIZE 100

struct priv_generate_key_pair {
    uint8_t privateKeyData[PRIVATE_KEY_DATA_SIZE];
    struct key_pair res;
};

typedef struct {
    bip32_path_with_curve_t key;
    uint8_t signed_hmac_key[MAX_SIGNATURE_SIZE];
    uint8_t hashed_signed_hmac_key[CX_SHA512_SIZE];
    uint8_t hmac[CX_SHA256_SIZE];
} apdu_hmac_state_t;

typedef struct {
    b2b_state state;
    bool initialized;
} blake2b_hash_state_t;

typedef struct {
    bip32_path_with_curve_t key;

    struct {
      enum validation_state vstate;
      parsed_baking_data_t v;
    } maybe_parsed_baking_data;

    struct {
      enum validation_state vstate;
      struct parsed_operation_group v;
    } maybe_ops;

    uint8_t message_data[DUNE_BUFSIZE];
    uint32_t message_data_length;
    buffer_t message_data_as_buffer;

    blake2b_hash_state_t hash_state;
    uint8_t final_hash[SIGN_HASH_SIZE];

    uint8_t magic_number;
    bool hash_only;
} apdu_sign_state_t;

typedef struct {
  void *stack_root;

  union {
    struct {
      bip32_path_with_curve_t key;
      cx_ecfp_public_key_t public_key;
    } pubkey;

    apdu_sign_state_t sign;

    struct {
      level_t reset_level;
    } baking;

    struct {
        bip32_path_with_curve_t key;
        chain_id_t main_chain_id;
        struct {
            level_t main;
            level_t test;
        } hwm;
    } setup;

    apdu_hmac_state_t hmac;

    struct {
      uint8_t new_baking_mode ;
    } set_baking_mode ;
  } u;

  struct {
    ui_callback_t ok_callback;
    ui_callback_t cxl_callback;

    uint32_t ux_step;
    uint32_t ux_step_count;

    uint32_t timeout_cycle_count;

    struct {
        char hwm[MAX_INT_DIGITS + 1]; // with null termination
        char pkh[PKH_STRING_SIZE];
        char chain[CHAIN_ID_BASE58_STRING_SIZE];
    } baking_idle_screens;

    struct {
      string_generation_callback callbacks[MAX_SCREEN_COUNT];
      const void *callback_data[MAX_SCREEN_COUNT];

#     ifdef TARGET_NANOX
      struct {
        char prompt[PROMPT_WIDTH + 1];
        char value[VALUE_WIDTH + 1];
      } screen[MAX_SCREEN_COUNT];
#     else
      char active_prompt[PROMPT_WIDTH + 1];
      char active_value[VALUE_WIDTH + 1];

      // This will and must always be static memory full of constants
      const char *const *prompts;
#     endif
    } prompt;
  } ui;

  struct {
    nvram_data new_data;  // Staging area for setting N_data

    char address_display_data[VALUE_WIDTH + 1];
  } baking_auth;

  struct {
    struct priv_generate_key_pair generate_key_pair;

    struct {
      cx_ecfp_public_key_t compressed;
    } public_key_hash;
  } priv;
} globals_t;

#define HAS_FLAG(v, flag) ( ( v & flag ) != 0 )
#define NOT_FLAG(v, flag) ( ( v & flag ) == 0 )

#define BAKING_FLAG 1
#define ALWAYS_FLAG 2
#define RAMWAT_FLAG 4

#define BAKING_MODE global.baking_auth.new_data.baking_mode
#define IS_BAKING_MODE() HAS_FLAG ( BAKING_MODE , BAKING_FLAG )
#define IS_MODE_ALWAYS() HAS_FLAG ( BAKING_MODE , ALWAYS_FLAG )
#define RAM_WATERMARK()  HAS_FLAG ( BAKING_MODE , RAMWAT_FLAG )

extern globals_t global;

extern unsigned int app_stack_canary; // From SDK

// Used by macros that we don't control.
#ifdef TARGET_NANOX
    extern ux_state_t G_ux;
    extern bolos_ux_params_t G_ux_params;
#else
    extern ux_state_t ux;
#endif
extern unsigned char G_io_seproxyhal_spi_buffer[IO_SEPROXYHAL_BUFFER_SIZE_B];

static inline void throw_stack_size() {
    uint8_t st;
    // uint32_t tmp1 = (uint32_t)&st - (uint32_t)&app_stack_canary;
    uint32_t tmp2 = (uint32_t)global.stack_root - (uint32_t)&st;
    THROW(0x9000 + tmp2);
}

#   ifdef TARGET_NANOX
        extern nvram_data const N_data_real;
#       define N_data (*(volatile nvram_data *)PIC(&N_data_real))
#    else
        extern nvram_data N_data_real;
#       define N_data (*(nvram_data*)PIC(&N_data_real))
#    endif

void update_baking_idle_screens(void);
high_watermark_t *select_hwm_by_chain(chain_id_t const chain_id, baker *const ram);

#define LOAD_FROM_NVRAM() \
   memcpy(&global.baking_auth.new_data, &N_data, \
          sizeof(global.baking_auth.new_data))

#define SAVE_TO_NVRAM() \
   nvm_write((void*)&N_data, &global.baking_auth.new_data, sizeof(N_data))

// Properly updates NVRAM data to prevent any clobbering of data.
// 'out_param' defines the name of a pointer to the nvram_data struct
// that 'body' can change to apply updates.

// We removed this line before `body` because it is always done on startup
//    LOAD_FROM_NVRAM();
// So, `global.baking_auth.new_data` is always up-to-date and we should
// never read N_data anymore.

#define UPDATE_NVRAM(out_name, body) ({                        \
    nvram_data *const out_name = &global.baking_auth.new_data; \
    body; \
    SAVE_TO_NVRAM(); \
    update_baking_idle_screens(); \
})

// This is supposed to replace any occurrence of `N_data`
#define CACHE_NVRAM global.baking_auth.new_data

// Allowed operations based on mode
static inline bool is_operation_allowed (enum operation_tag tag) {
  switch (tag) {
  case OPERATION_TAG_NONE:
    return false;

  case OPERATION_TAG_DELEGATION:
  case OPERATION_TAG_REVEAL:
    return true;

  case OPERATION_TAG_PROPOSAL:
  case OPERATION_TAG_BALLOT:
  case OPERATION_TAG_ORIGINATION:
  case OPERATION_TAG_TRANSACTION:
#ifdef DUNE_APP
  case OPERATION_TAG_COLLECT_CALL:
  case OPERATION_TAG_DUNE_MANAGER:
#endif
    return ! IS_BAKING_MODE();
  }
}
